package DAOs;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ArticleDAO implements AutoCloseable {
    private final Connection conn;


    public ArticleDAO() throws IOException, SQLException {
        this.conn = HikariConnectionPool.getConnection();
    }

    //todo get what we want from the DB by different methods
    public List<Article> getAllArticles() throws SQLException {
        List<Article> articles = new ArrayList<>();
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM clin864.blog_articles")) {
            try (ResultSet r = stmt.executeQuery()) {
                while (r.next()) {
                    Article article = new Article();
                    article.setArticleId(r.getInt("article_id"));
                    article.setTitle(r.getString("title"));
                    article.setContent(r.getString("content"));
                    article.setModifiedDateAndTime(r.getString("modified_date"));
                    article.setStatus((r.getString("status")));
                    article.setUsername(r.getString("username"));

                    articles.add(article);


                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return articles;
    }


    public List<Article> getArticlesByName(String name) throws SQLException {
        List<Article> articles = new ArrayList<>();
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM blog_articles WHERE username LIKE ?")) {

            stmt.setString(1, name);
            try (ResultSet r = stmt.executeQuery()) {
                while (r.next()) {
                    Article article = new Article();
                    article.setArticleId(r.getInt("article_id"));
                    article.setTitle(r.getString("title"));
                    article.setContent(r.getString("content"));
                    article.setModifiedDateAndTime(r.getString("modified_date"));
                    article.setStatus((r.getString("status")));
                    article.setUsername(r.getString("username"));
                    articles.add(article);
                }

                return articles;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return articles;
    }


    public List<Article> getArticlesByTitle(String title) throws SQLException {
        List<Article> articles = new ArrayList<>();
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM blog_articles WHERE title LIKE ?")) {

            stmt.setString(1, title);
            try (ResultSet r = stmt.executeQuery()) {
                while (r.next()) {
                    Article article = new Article();
                    article.setArticleId(r.getInt("article_id"));
                    article.setTitle(r.getString("title"));
                    article.setContent(r.getString("content"));
                    article.setModifiedDateAndTime(r.getString("modified_date"));
                    article.setStatus((r.getString("status")));
                    article.setUsername(r.getString("username"));
                    articles.add(article);
                }

                return articles;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return articles;
    }

    public List<Article> getArticlesByDate(String modified_date) throws SQLException {
        List<Article> articles = new ArrayList<>();
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM blog_articles WHERE modified_date LIKE ?")) {

            stmt.setString(1, modified_date);
            try (ResultSet r = stmt.executeQuery()) {
                while (r.next()) {
                    Article article = new Article();
                    article.setArticleId(r.getInt("article_id"));
                    article.setTitle(r.getString("title"));
                    article.setContent(r.getString("content"));
                    article.setModifiedDateAndTime(r.getString("modified_date"));
                    article.setStatus((r.getString("status")));
                    article.setUsername(r.getString("username"));
                    articles.add(article);
                }

                return articles;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return articles;
    }


    public void createNewArticle(Article article){

        try(  PreparedStatement stmt = conn.prepareStatement("INSERT INTO clin864.blog_articles (title, content, username)  VALUES (?,?,?)");) {

            {

                stmt.setString(1, article.getTitle());
                stmt.setString(2, article.getContent());
                stmt.setString(3, article.getUsername());
                stmt.executeUpdate();



            }

        } catch (SQLException e) {
            e.getMessage();
        }
    }


    public void modifyArticle( HttpSession session){

        String name=(String)session.getAttribute("titleToGive");
        String newContent=(String)session.getAttribute("textToGive");
        int id=(Integer)session.getAttribute("iDToGive");


//

        try(PreparedStatement stmt = conn.prepareStatement(" UPDATE clin864.blog_articles SET content=?,title=? WHERE title=?")) {

            ;
            {
                stmt.setString(1,newContent);
                stmt.setString(2,name);
                stmt.setInt(3,id);

                stmt.executeUpdate();
            }

        } catch (SQLException e) {
            e.getMessage();
        }




    }



    public void deleteArticle(String name) throws SQLException {


        int id=Integer.parseInt(name);
        System.out.println(name);
        try( PreparedStatement stmt = conn.prepareStatement("DELETE FROM clin864.blog_articles WHERE blog_articles.article_id LIKE ?")) {
            {
               stmt.setInt(1,id);
               stmt.executeUpdate();

            }

        } catch (SQLException e) {
            e.getMessage();
        }


    }


    @Override
    public void close() throws Exception {
        this.conn.close();
    }
}
