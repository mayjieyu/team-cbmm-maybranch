package DAOs;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAO implements AutoCloseable {
    private final Connection conn;


    public UserDAO() throws IOException, SQLException {
        this.conn = HikariConnectionPool.getConnection();
    }

    @Override
    public void close() throws Exception {
        this.conn.close();
    }

    //get what we want from the DB by different methods

    public User getAllUserInfoByName(String username) throws SQLException {
        User user = new User();
        try (PreparedStatement stmt = conn.prepareStatement("SELECT DISTINCT * FROM blog_users WHERE username = ?")) {
            stmt.setString(1, username);
            try (ResultSet r = stmt.executeQuery()) {
                if (r.next()){
                    user.setUsername(r.getString("username"));
                    user.setFname(r.getString("fname"));
                    user.setLname(r.getString("lname"));
                    user.setEmail(r.getString("email"));
                    user.setDob(r.getString("dob"));
                    user.setCountry(r.getString("country"));
                    user.setAvatar(r.getString("avatar"));
                    user.setDescrp(r.getString("descrp"));
                    user.setRole(r.getString("role"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return user;
    }

    public String getPassword(String username) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("SELECT DISTINCT password " +
                "FROM blog_users " +
                "WHERE username = ?")) {
            stmt.setString(1, username);
            try (ResultSet r = stmt.executeQuery()) {
                if (r.next()) {
                    return r.getString(1);
                } else {
                    return null;
                }
            }
        }
    }

    public void createNewUser(User user) throws SQLException{
        try(PreparedStatement stmt = conn.prepareStatement("INSERT INTO blog_users" +
                "(username, password, fname, lname, email, dob, country, descrp, avatar) " +
                "VALUES (?, ?, ?, ? ,? , ?, ?, ?, ?)")){
            stmt.setString(1,user.getUsername());
            stmt.setString(2, user.getPassword());
            stmt.setString(3, user.getFname());
            stmt.setString(4, user.getLname());
            stmt.setString(5, user.getEmail());
            stmt.setString(6, user.getDob());
            stmt.setString(7, user.getCountry());
            stmt.setString(8, user.getDescrp());
            stmt.setString(9, user.getAvatar());
            stmt.executeUpdate();
        }
    }

    public void modifyUser(User user){
        String sql = "UPDATE blog_users SET fname = ?, lname = ?, email = ?, dob = ?, " +
                "country = ?, descrp = ? WHERE username = ?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, user.getFname());
            stmt.setString(2, user.getLname());
            stmt.setString(3, user.getEmail());
            stmt.setString(4, user.getDob());
            stmt.setString(5, user.getCountry());
            stmt.setString(6, user.getDescrp());
            stmt.setString(7, user.getUsername());
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
