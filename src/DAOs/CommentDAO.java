package DAOs;



import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CommentDAO implements AutoCloseable {
    private final Connection conn;


    public CommentDAO() throws IOException, SQLException {
        this.conn = HikariConnectionPool.getConnection();
    }

    //todo get what we want from the DB by different methods
    public List<Comment> getAllCommentsByArticleId(int id ) throws SQLException {
        List<Comment> comments = new ArrayList<>();
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM blog_comments WHERE article_id LIKE ?")) {
                stmt.setInt(1,id);
                stmt.execute();
            try (ResultSet r = stmt.executeQuery()) {
                while (r.next()) {
                    Comment comment = new Comment();
                    comment.setCommentId(r.getInt("comment_id"));
                    comment.setArticleId(r.getInt("article_id"));
                    comment.setReferenceId(r.getInt("reference_id"));
                    comment.setContent(r.getString("content"));
                    comment.setCreatedDate(r.getString("created_date"));
                    comment.setUsername(r.getString("username"));
                    comments.add(comment);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return comments;
    }




    public List<Comment> getAllComments( ) throws SQLException {

        List<Comment> comments = new ArrayList<>();
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM blog_comments")) {
            try (ResultSet r = stmt.executeQuery()) {
                while (r.next()) {
                    Comment comment = new Comment();
                    comment.setCommentId(r.getInt("comment_id"));
                    comment.setArticleId(r.getInt("article_id"));
                    comment.setReferenceId(r.getInt("reference_id"));
                    comment.setContent(r.getString("content"));
                    comment.setCreatedDate(r.getString("created_date"));
                    comment.setUsername(r.getString("username"));
                    comments.add(comment);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return comments;
    }


    public void createNewComment(Comment comment){


        try(PreparedStatement stmt = conn.prepareStatement("INSERT INTO clin864.blog_comments (article_id, content, username)  VALUES (?,?,?)")) {

                stmt.setInt(1, comment.getArticleId());
                stmt.setString(2, comment.getContent());
                stmt.setString(3, comment.getUsername());
                stmt.execute();

        } catch (SQLException e) {
            e.getMessage();
        }
    }

    public  void deleteComment(int id) throws SQLException {

        try( PreparedStatement stmt = conn.prepareStatement("DELETE FROM clin864.blog_comments WHERE blog_comments.comment_id LIKE ?")) {

                stmt.setInt(1, id);
                stmt.execute();



        } catch (SQLException e) {
            e.getMessage();
        }

    }
    @Override
    public void close() throws Exception {
        this.conn.close();
    }
}
