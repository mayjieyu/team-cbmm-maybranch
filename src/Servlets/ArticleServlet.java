package Servlets;

import DAOs.Article;
import DAOs.ArticleDAO;
import DAOs.Comment;
import DAOs.CommentDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


public class ArticleServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        HttpSession myUser= req.getSession();
        displayArticlesList(req, resp);

//if (req.getParameter("newArticle").equals("true")){
//
//}

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        Cookie[] cookies = req.getCookies();

        for (int i = 0; i < cookies.length; i++) {

            String value = cookies[i].getValue();

            if (value.equals("true")){


                req.getSession().setAttribute("textToGive",req.getParameter("editTextArea"));
                req.getSession().setAttribute("titleToGive",req.getParameter("editTitle"));
                req.getSession().setAttribute("iDToGive",req.getParameter("editID"));
                cookies[i].setMaxAge(0);
                HttpSession session=req.getSession();
                modifyArticle(session);



            }

            if (value.equals("newArticle")){

                req.getSession().setAttribute("newContent",req.getParameter("newContent"));
                req.getSession().setAttribute("newTitle",req.getParameter("newTitle"));
                cookies[i].setMaxAge(0);
                makeNewArticle(req,resp);
            }

            if (value.equals("delete")){
                String deleteTitle=req.getParameter("articleID");
                cookies[i].setMaxAge(0);
                deleteArticle(req,resp,deleteTitle);
            }

// TODO: 29/05/18 delete these cookies after you use them! 
        }


//       makeNewArticle(req,resp);
//        req.getSession().removeAttribute("articles");

        try(ArticleDAO updatedArticles=new ArticleDAO()){
           List<Article> toSupply=updatedArticles.getAllArticles();
           req.getSession().setAttribute("articles",toSupply);

        }catch (Exception e){
            e.getMessage();
        }

        req.getRequestDispatcher("/jsp/mainPage.jsp").forward(req, resp);


    }


    private void modifyArticle(HttpSession session){

        try(ArticleDAO toFind=new ArticleDAO()) {
            String name=(String)session.getAttribute("titleToGive");
            String newContent=(String)session.getAttribute("textToGive");



            toFind.modifyArticle(session);

            // TODO: 26/05/18 don't we have to do a get method after this? In that case, we'll need request and response objects



            // TODO: 24/05/18 tidy this up. It is unacceptable that an article of the same name might be modified when modifing andther article

        }catch (Exception e){
            e.printStackTrace();
        }


    }


    private void makeNewArticle(HttpServletRequest req, HttpServletResponse resp){


        HttpSession session=req.getSession();
        try (ArticleDAO makeNewArticle=new ArticleDAO())     {
            Article article=new Article();

           String newContent=(String) session.getAttribute("newContent");
            String newTitle=(String)session.getAttribute("newTitle");
            String userName=(String)session.getAttribute("username");

            article.setTitle(newTitle);
            article.setContent(newContent);
            article.setUsername(userName);



            makeNewArticle.createNewArticle(article);

//            req.getRequestDispatcher("/jsp/mainPage.jsp").forward(req, resp);
        }catch (Exception e){
            e.getMessage();
        }
    }

    private void displayArticlesList(HttpServletRequest request, HttpServletResponse response) {


        try (ArticleDAO dao = new ArticleDAO()) {

            List<Article> articles = dao.getAllArticles();

            // Adding the article list to the request object
            request.setAttribute("articles", articles);
            try(CommentDAO toUse=new CommentDAO()){
                List<Comment> toTest=toUse.getAllComments();
                request.setAttribute("comments",toTest);



            }
//            CommentDAO toUse=new CommentDAO();
//            List<Comment> toTest=toUse.getAllComments();
//
            request.getRequestDispatcher("/jsp/mainPage.jsp").forward(request, response);

        }catch (Exception e){
            e.getMessage();
        }
    }

    private void displaySingleArticle(HttpServletRequest request, HttpServletResponse response, String name){

        try (DAOs.ArticleDAO dao = new ArticleDAO()) {

            List<Article> article = dao.getArticlesByName(name);
            if (article == null) {
                response.sendError(404);
                return;
            }

            // Adding the article to the request object so that our article.jsp page can access it
            request.setAttribute("Article", article);

            // Redirect to /WEB-INF/example03/article_jstl.jsp, which will now have access to the article through its
            // request object, due to the line above.
            request.getRequestDispatcher("/jsp/mainPage.jsp").forward(request, response);

        }catch (Exception e){
            e.getMessage();
        }
    }


private void deleteArticle(HttpServletRequest request, HttpServletResponse response, String name){
        
        //this method creates an ArticleDAO and from there, calls the delete article method to remove it from the database
        
    try ( ArticleDAO toControl=new ArticleDAO())                     {

        toControl.deleteArticle(name);


        //redirects to main page
//        request.getRequestDispatcher("/jsp/mainPage.jsp").forward(request, response);
//        // TODO: 26/05/18 check whether this code allows articles to be seem after being modified
//        doGet(request,response);


        // TODO: 23/05/18 tidyup this try/catch block 
    } catch (Exception e) {
        e.printStackTrace();
    }
  

}





}
