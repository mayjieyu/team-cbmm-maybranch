package Servlets;

import DAOs.User;
import DAOs.UserDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class UserInfoServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String userName=(String)req.getSession().getAttribute("userName");

        try(UserDAO getInfo=new UserDAO()) {


            User toReturn=getInfo.getAllUserInfoByName(userName);
            req.getSession().setAttribute("user", toReturn);
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/UpdateInfo.jsp");
            dispatcher.forward(req, resp);


        }catch (Exception e){
            e.getMessage();
        }


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String userName=(String)req.getSession().getAttribute("userName");
        try (UserDAO getInfo=new UserDAO()){

            User toReturn=new User();

           toReturn.setFname(req.getParameter("fname"));
            toReturn.setLname(req.getParameter("lname"));
            toReturn.setPassword(req.getParameter("password"));
            toReturn.setEmail(req.getParameter("email"));
            toReturn.setDob(req.getParameter("dob"));
            toReturn.setCountry(req.getParameter("country"));
           toReturn.setAvatar(req.getParameter("avatar"));
            toReturn.setRole(req.getParameter("role"));

            getInfo.modifyUser(toReturn);

        }catch (SQLException e){
            e.getMessage();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
