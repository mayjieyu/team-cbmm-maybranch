package Servlets;
/**
 * Description: Servlets.LoginServlet servlet:
 * - login checking
 * ----------------------------------------------------------
 * Version  |   Date        |   Created by          |   Description
 * v1       |   21/05/2018  |   Chinchien           | username & pw checking, create a session attribute "username"
 * v2       |   22/05/2018  |   Chinchien & Massie  | get & set user's info & articles
 * v3       |   24/05/2018  |   Chinchien           | We change the design, after logging in, should direct user to the main page, so need to move v2 into another servlet
 * | Due to some connection errors to the DB, I change some codes on this one, DAO, and login html file
 */

import DAOs.UserDAO;
import sun.security.util.Password;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;

public class LoginServlet extends HttpServlet {
    //if we get any error msg, set session's attribute "errorMsg" to the message that we want to show up
    //when back to the login page
    private String errorMsg;
    Cookie cookie = new Cookie("errorMsg", ""); //initialise the cookie "errorMsg"
     public static final byte[] SALT= {45, 57, 11, 88};
    public static final int ITERATIONS=10;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //users will be direct to the main page (ArtcileServlet) whatever they clicked the login or logout button
        //logout
        String status = request.getParameter("status");
        if (status != null && status.equals("logout")) {
            request.getSession().setAttribute("username", null);




            response.sendRedirect("Articles");
        } else {
            //login
            String username = request.getParameter("username");
            String inputPassword = request.getParameter("password");
            String password = null;
            try (UserDAO userdao = new UserDAO()) {
                //check PW
                password = userdao.getPassword(username);
                if (password == null) { //if the user does not exit
                    //direct back to the login page, and store an error message in cookie
                    //PS: a cookie can only store 32 chars
//                errorMsg = "incorrect username";
                    errorMsg = "1";
                    cookie = new Cookie("errorMsg", errorMsg);
                    response.addCookie(cookie);
                    response.sendRedirect("/login.html");
                }
                //check if the password exits: the input pw need to match the hashed pw
                if (password != null) {
                    //todo pw security



//                byte[] bytecode = Passwords.base64Decode(password);
//                boolean isMatched = Passwords.isInsecureHashMatch(inputPassword, bytecode);

                   char[] input= inputPassword.toCharArray();
                   byte[] expectedPassword= Passwords.base64Decode(password);
                    boolean isMatched=Passwords.isExpectedPassword(input,SALT,ITERATIONS,expectedPassword);

// TODO: 29/05/18 convert stored passwords to hashes.



//                    boolean isMatched = password.equals(inputPassword);
                    if (!isMatched) {
//                    errorMsg = "incorrect password";
                        errorMsg = "2";
                        cookie = new Cookie("errorMsg", errorMsg);
                        response.addCookie(cookie);
                        response.sendRedirect("/login.html");
                    } else {
                        request.getSession().setAttribute("username", username);
                        request.getSession().setMaxInactiveInterval(480);

                        //direct to the main page
                        response.sendRedirect("Articles");
                    }
                }
            } catch (SQLException e) {
//            errorMsg = "Our system is not available now";
                errorMsg = "3";
                cookie = new Cookie("errorMsg", errorMsg);
                response.addCookie(cookie);
                response.sendRedirect("/login.html");
            } catch (Exception e) {
//            errorMsg = "Our system is not available now";
                errorMsg = "3";
                cookie = new Cookie("errorMsg", errorMsg);
                response.addCookie(cookie);
                response.sendRedirect("/login.html");
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
