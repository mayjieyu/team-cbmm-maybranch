package Servlets;

import DAOs.Article;
import DAOs.ArticleDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@WebServlet(name = "SearchServlet")
public class SearchServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //解决乱码
        request.setCharacterEncoding("utf-8");
        //接受表单内容
        String command = "%" + request.getParameter("command")+"%";
        String date = request.getParameter("date")+"%";
//        String date = "2018-05-28 01:28:40";

        System.out.println("dte"+ date);


        //向页面传值
        request.setAttribute("command",command);
        request.setAttribute("date",date);
        List<Article> articles = new ArrayList<>();

        if (request.getParameter("command")!= null) {
            try(ArticleDAO articleDAO=new ArticleDAO()) {

                articles = articleDAO.getArticlesByName(command);
                articles.addAll(articleDAO.getArticlesByTitle(command));

                request.setAttribute("articles",articles);

            } catch (SQLException e) {
                System.out.println("no Article Name");
                e.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (request.getParameter("date")!= null) {
            try (ArticleDAO articleDAO=new ArticleDAO()){

                articles = articleDAO.getArticlesByDate(date);

                request.setAttribute("articles",articles);

            } catch (SQLException e) {
                System.out.println("no Article Date");
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }



//        ListService listService = new ListService();
//        //查询消息列表并传给页面
//        request.setAttribute("messageList",listService.queryMessageList(command,description));
//        //向页面跳转
        request.getRequestDispatcher("/jsp/mainPage.jsp").forward(request,response);


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
