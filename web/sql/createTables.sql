DROP TABLE IF EXISTS blog_article_topic;
DROP TABLE IF EXISTS blog_topics;
DROP TABLE IF EXISTS blog_comments;
DROP TABLE IF EXISTS blog_articles;
DROP TABLE IF EXISTS blog_users;

CREATE TABLE IF NOT EXISTS blog_users(
  username VARCHAR(64) NOT NULL,
  password VARCHAR(100) NOT NULL,
  fname VARCHAR(64),
  lname VARCHAR(64),
  email VARCHAR(100),
  dob DATE,
  country VARCHAR(2),
  descrp VARCHAR (200),
  avatar VARCHAR(120),
  role VARCHAR(25),
  PRIMARY KEY (username)
);

alter table blog_users modify column avatar varchar(500);

CREATE TABLE IF NOT EXISTS blog_themes(
  themes_id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(20) NOT NULL,
  css_path VARCHAR (260),
  PRIMARY KEY (themes_id)
);


CREATE TABLE IF NOT EXISTS blog_blogs(
  blog_id INT NOT NULL AUTO_INCREMENT,
  title VARCHAR(60) NOT NULL,
  descrp VARCHAR (200),
  username VARCHAR(64) NOT NULL,
  theme_id INT,
  PRIMARY KEY (blog_id),
  FOREIGN KEY (username) REFERENCES blog_users(username),
  FOREIGN KEY (theme_id) REFERENCES blog_themes (themes_id)
);

CREATE TABLE IF NOT EXISTS blog_articles (
  article_id INT NOT NULL AUTO_INCREMENT,
  title VARCHAR(60) NOT NULL,
  content TEXT,
  modified_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  status VARCHAR(20),
  username VARCHAR(64) NOT NULL,
  PRIMARY KEY (article_id),
  FOREIGN KEY (username) REFERENCES blog_users (username)
);


CREATE TABLE IF NOT EXISTS blog_comments (
  comment_id INT NOT NULL AUTO_INCREMENT,
  article_id INT NOT NULL,
  reference_id INT,
  content VARCHAR(200),
  created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  username VARCHAR(64) NOT NULL,
  PRIMARY KEY (comment_id),
  FOREIGN KEY (article_id) REFERENCES blog_articles(article_id),
  FOREIGN KEY (username) REFERENCES blog_users(username)
);

CREATE TABLE IF NOT EXISTS blog_topics(
  topic_id INT NOT NULL AUTO_INCREMENT,
  topic VARCHAR(20) NOT NULL,
  PRIMARY KEY (topic_id)
);

CREATE TABLE IF NOT EXISTS blog_article_topic(
  topic_id INT NOT NULL AUTO_INCREMENT,
  topic VARCHAR(20) NOT NULL,
  PRIMARY KEY (topic_id)
);

