<%--
  Created by IntelliJ IDEA.
  User: may
  Date: 22/05/2018
  Time: 3:13 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <%--bootstrap--%>

    <style>

        .commentArticleId{
            display: none;
        }

        #registerModal {
            width: 800px;
        }

        #editTextArea {
            display: none;
        }

        .save {
            display: none;
        }



        .comment {
            /*for styling comments in the table*/
            width: 250px;
            height: 35px;
            background-color: forestgreen;
            margin: 5px;
            border-radius: 5px;
            text-align: left;
            padding: 10px;
            margin-left: 10px;
            display: none;
        }


        .newcomment {
            /*for styling comments in the table*/
            width: 250px;
            height: 35px;
            background-color: forestgreen;
            margin: 5px;
            border-radius: 5px;
            text-align: left;
            padding: 10px;
            margin-left: 10px;

        }

        #articleModal {
            padding: 20px;
        }

       


        .smallCommentBtn {
            width: 100px;
            height: 35px;
            font-size: 10px;
            display: inline;
        }

        .tinyComment {
            transform: translate(25px, 0px);
            width: 85px;
            height: 35px;
            background-color: #3B5998;
            border-radius: 20px
        }

        .tinyCommentBtn {
            transform: translate(10px, 0px);
            width: 75px;
            height: 25px;
        }

        .smallText {
            display: inline;
            height: 35px;
        }

        .tinyText {
            width: 75px;
            height: 25px;
            display: inline;
        }

        #commentArea {
            padding: 10px;
            margin: 10px;
        }

        #editTitle {
            display: none;
        }

        #saveButton {
            display: none;
        }

    </style>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <%--css--%>
    <link rel="stylesheet" type="text/css" href="<c:url value='../css/page.css' />"/>
</head>
<body class="bodyStyle">
<header>
    <%@ include file="navbar.jsp" %>
</header>

<div class="container">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">

            <div class="item active">
                <img src="image/image01.png" alt="Technology" style="width:100%;">
                <div class="carousel-caption">
                    <h3>Technology</h3>
                    <p>Technology is connecting the World at a speed of 'speed of 'light' '</p>
                </div>
            </div>

            <div class="item">
                <img src="image/image02.png" alt="Health" style="width:100%;">
                <div class="carousel-caption">
                    <h3>Health</h3>
                    <p>Health is Wealth!</p>
                </div>
            </div>

            <div class="item">
                <img src="image/image03.png" alt="Education" style="width:100%;">
                <div class="carousel-caption">
                    <h3>Education</h3>
                    <p>Learning new things 'FastPace' is like taking an daily dose of Adrenaline!!</p>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="container">
    <table class="tab1">
        <tbody>
        <form action="SearchServlet" method="post">
        <tr>
            <%--<td width="90" align="right">Search</td>--%>
            <td>
                <input name="command" type="text" class="allInput" value="${command}"/>
            </td>
            <td width="85" align="right"><input type="submit" class="tabSub" value="Search" /></td>
        </tr>
        </form>
        <form action="SearchServlet" method="post">
            <input type="date" name="date" value="${date}">
            <input type="submit">
        </form>
        </tbody>
    </table>
</div>
<br>

<div class="container container-margin">
    <c:choose>
    <c:when test="${fn:length(articles) gt 0}">

    <c:forEach var="article" items="${articles}">


            <%--<h3 class="panel-title pull-left"><a href="?article=${article.id}"></a></h3>--%>

    <div class="row contentCard02" id="${article.articleId}">
        <div class="col-md-12">
            <h5 class="card-title">${article.title}</h5>
            <p class="author">By / <span>${article.username}</span></p>
                <%--only show short content with 200 chars--%>
            <hr class="line">
            <p class="card-text">${fn:substring(article.content, 0, 199)}</p>

                        <%--<p class="card-text">${article.content}</p>--%>
                    <%--<a href="#" class="btn btn-primary">Load Full Article</a>--%>




                <%--<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#articleModal"   >--%>
                <%--&lt;%&ndash;data-content="${article.content}" data-title="${article.title}"    &ndash;%&gt;--%>
                <%--Load Article--%>
                <%--</button>--%>
        </div>
        <button type="button" class="btn btn-danger load" data-toggle="modal" data-target="#articleModal"
                data-username="${article.username}" data-content="${article.content}" data-title="${article.title}"
                data-id="${article.articleId}" >
            Load Article
        </button>

            <%--<c:if test = "${articlecreator==userUsername}">--%>
            <%--<c:--%>
        <%--<%--%>
            <%--String owner = (String) session.getAttribute("username");--%>
            <%--session.setAttribute("owner", owner);--%>
        <%--%>--%>
            <%--%>--%>
            <%--<% String username= %>--%>
        <c:choose>
            <c:when test="${username == article.username}">
                <button type="button" id="deleteButton" class="btn btn-danger" onclick="deleteArticle(${article.articleId})"
                        data-articleID="">Delete Article
                </button>
            </c:when>
            <c:otherwise>
                <p>hello</p>
            </c:otherwise>
        </c:choose>
        <%--<c:if test="${article.username == userUsername} ">--%>

            <%--<button type="button" id="deleteButton" class="btn btn-danger" onclick="deleteArticle(${article.articleId})"--%>
                    <%--data-articleID="">Delete Article--%>
            <%--</button>--%>
        <%--</c:if>--%>
            <%--</c:if>--%>


    </div>

    </c:forEach>
    </c:when>
    <c:otherwise>
    <p>No articles!</p>
    </c:otherwise>
    </c:choose>

    <br>

    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<%@include file="articleModal.jsp"%>
        <%@include file="newArticleModal.jsp"%>
<%@include file="scripts.jsp"%>






</body>
</html>
