<script>



    $('#articleModal').on('show.bs.modal', function (event) {

        var modal = $(this);
        var button = $(event.relatedTarget); // Button that triggered the modal
        var articleID = button.data('id');

        var button = $(event.relatedTarget); // Button that triggered the modal

        var titleData = button.data('title');
        var contentData = button.data('content');
        var username = button.data('username');


        // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.

        modal.find('.modal_title').text(titleData);
        modal.find('.modal_txt').text(contentData);
        modal.find('#editTextArea').text(contentData);
        modal.find('#editTitle').val(titleData);
        modal.find('.modal_user').text(username);
        modal.find('#modal_id').text(articleID);


        var commentArticleId = document.getElementsByClassName("commentArticleId");
        // var articleId = commentArticleId.toString();
        var comments=document.getElementsByClassName("comment");

        for ( var i = 0; i < comments.length; i++) {
            var id = commentArticleId[i].innerText;
            console.log("commentarticle id="+id);
            console.log("articleID: "+articleID);


            var checkComment=comments[i];

            // var commnetArticleId = comments[i].articleID;
            // var commentArticleID=checkComment.data('id');

            // console.log("the article id for this comment is "+commentArticleID);
            // if(commentArticleID.equals(articleID) ){

            if (!(id!=articleID)){
                checkComment.style.display="block";
                // }
            }else{
                checkComment.style.display="none";
            }
        }

    })



    function displayEdit() {

        var textarea = $("#articleModal #editTextArea");
        var savebutton = $("#articleModal #saveButton");
        var titlearea = $("#articleModal #editTitle");

        textarea.css("display", "block");
        savebutton.css("display", "block");
        titlearea.css("display", "block");


    }


    function leaveComment() {
        var comment = $('#articleModal #commentTextArea');
        var myComment = comment.val();
        var articleID = $('#articleModal #modal_id');
        var articleIDText = articleID.text();
        console.log(articleIDText);

        // console.log(comment);

        var commentArea = $("#articleModal #commentArea");

        commentArea.append("<div class=\"newcomment\" >" + myComment + "</div>");
        // commentArea.append("<button type=\"button\" class=\"btn btn-primary smallCommentBtn\" onclick=\"leaveNestedComment()\" id=\"commentButton\">Comment</button><br>");
        // commentArea.append("<textarea class=\"smallText\" rows='1' cols='35'></textarea> ");

        document.cookie = "edit=create";


        commentArea.css("background-color", "grey", "border-radius", "30px");

        var commentData = {text: myComment, articleID: articleIDText};
        $.ajax({
            type: 'post',
            url: "/Comment",
            data: commentData,
            dataType: "text",
            success: function (resultData) {


                alert("Save Complete")
            }
        });


    }

    function leaveNestedComment() {
        var comment = $('#articleModal #nestedComment');
        var myComment = comment.val();

        console.log(comment);

        var commentArea = $("#articleModal #commentArea");

        commentArea.append("<div class=\"tinyComment\" >" + myComment + "</div>");
        commentArea.append("<button type=\"button\" class=\"btn btn-primary tinyCommentBtn\" onclick=\"leaveNestedComment()\" id=\"commentButton\">Comment</button><br>");
        commentArea.append("<textarea class=\"tinyText\" rows='1' cols='35'></textarea> ");


        console.log(document.getElementById("commentArea"))

    }


    function deleteArticle(articleIDz) {

        alert(articleIDz);

        var articleID = articleIDz;
        console.log("the button says " + articleID);
        if (confirm("Are you sure you want to delete this Article?")) {
            document.cookie = "edit=delete";


            var articleDeletionData = {articleID: articleID};
            $.ajax({
                type: 'post',
                url: "/Articles",
                data: articleDeletionData,
                dataType: "text",
                success: function (resultData) {

                    alert("Deletion Complete")
                }
            });

            $('#' + articleID).css("display", "none");


        }


    }

    function editCookie() {
        document.cookie = "edit=true";
    }

    function makeNewArticle() {
        document.cookie = "edit=newArticle";


    }



    function deleteComment( commentId) {


        document.cookie = "edit=delete";

        var button = $(event.relatedTarget); // Button that triggered the modal

        var commentDataButton = button.data('cID');

        $('#' + commentId).css("display", "none");
        $('#' + commentId + "Button").css("display", "none");


        var commentData = {commentID: commentId};
        $.ajax({
            type: 'post',
            url: "/Comment",
            data: commentData,
            dataType: "text",
            success: function (resultData) {

                alert("Deletion Complete")
            }
        });





    }

</script>
